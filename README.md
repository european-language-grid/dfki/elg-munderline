# MunderLine ELG Wrapper

This is a simple [Spring Boot](https://spring.io/projects/spring-boot) based REST service that wraps the MunderLine multilingual pipeline with the [ELG API](https://gitlab.com/european-language-grid/platform/elg-apis).

MunderLine (The Multilingual Universal Dependency and Relation Pipeline) provides a multilingual pipeline that applies the following processing steps on an input text:

* tokenization

* part-of-speech tagging

* morphology tagging

* dependency parsing

* named entity recognition

Tokenization (and sentence border recognition) is done by a simple tokenizer. Part-of-speech tagging, morphology tagging as well as named entity recognition is done by the [GNTagger](https://github.com/DFKI-MLT/gnt) using statistical-based models. Dependency parsing is done by the [MDParser](https://github.com/gueneumann/MDParser), again using a statistical-based model.

Currently, MunderLine supports English, German, Spanish and Greek (the latter two without named entity recognition) but can easily be extended for other languages.

The pipeline provided here contains models trained using the following linguistic resources:

* [Universal Dependencies](https://universaldependencies.org/) (part-of-speech tagging, morphology tagging and dependency parsing for all supported languages)

* [CoNLL-2003](https://www.clips.uantwerpen.be/conll2003/ner/) (named entity recognition for English)

* [GermEval 2014](https://sites.google.com/site/germeval2014ner/data) (named entity recognition for German)

* [MarLiN Cluster](http://cistern.cis.lmu.de/marmot/naacl2015/) (named entity recognition for English and German)

The service accepts a JSON [TextRequest](https://gitlab.com/european-language-grid/platform/elg-apis/blob/master/doc/T2.5-api-design.md#text-requests) on port 8080 and returns a JSON [AnnotationsResponse](https://gitlab.com/european-language-grid/platform/elg-apis/blob/master/doc/T2.5-api-design.md#annotations-response) containing the text token's annotation. The following languages are supported: `de`, `en`, `es`, `el`. For each language, a separate endpoint is provided.

The service can be tested using the provided example query `text-request.json` with the following curl command:
```
curl -H "Content-Type: application/json" -d @text-request.json http://localhost:8080/en
```

The content of `text-request.json`, containing the text content to tokenize and annotate:
```
{
  "type": "text",
  "content": "Angela Merkel lives in Berlin."
}
```

The corresponding JSON annotations response with the text token's annotation and recognized text units:
```
{
  "response": {
    "type": "annotations",
    "annotations": {
      "tokens": [{
          "start": 0,
          "end": 6,
          "features": {
            "index": "1",
            "token": "Angela",
            "pos": "PROPN",
            "morph": "Number=Sing",
            "dep-head": "2",
            "dep-rel": "compound",
            "ner": "B-PER"
          }
        }, {
          "start": 7,
          "end": 13,
          "features": {
            "index": "2",
            "token": "Merkel",
            "pos": "PROPN",
            "morph": "Number=Sing",
            "dep-head": "3",
            "dep-rel": "compound",
            "ner": "L-PER"
          }
        }, {
          "start": 14,
          "end": 19,
          "features": {
            "index": "3",
            "token": "lives",
            "pos": "NOUN",
            "morph": "Number=Plur",
            "dep-head": "0",
            "dep-rel": "root",
            "ner": "O"
          }
        }, {
          "start": 20,
          "end": 22,
          "features": {
            "index": "4",
            "token": "in",
            "pos": "ADP",
            "morph": "_",
            "dep-head": "5",
            "dep-rel": "case",
            "ner": "O"
          }
        }, {
          "start": 23,
          "end": 29,
          "features": {
            "index": "5",
            "token": "Berlin",
            "pos": "PROPN",
            "morph": "Number=Sing",
            "dep-head": "3",
            "dep-rel": "nmod",
            "ner": "U-LOC"
          }
        }, {
          "start": 29,
          "end": 30,
          "features": {
            "index": "6",
            "token": ".",
            "pos": "PUNCT",
            "morph": "_",
            "dep-head": "3",
            "dep-rel": "punct",
            "ner": "O"
          }
        }
      ],
      "text_units": [{
          "start": 0,
          "end": 30
        }
      ]
    }
  }
}
```
