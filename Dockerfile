# define args to be used in following language specific builds
ARG user=elg
ARG group=elg
ARG uid=1001
ARG gid=1001
ARG app_home=/MunderLine
ARG version=1.0.0


FROM maven:3.6.3-ibmjava-8-alpine as buildstage

COPY pom.xml /tmp/
COPY src/test /tmp/src/test/
COPY src/main/java /tmp/src/main/java/
COPY src/main/resources/mdp.conf /tmp/src/main/resources/
COPY src/main/resources/logback.xml /tmp/src/main/resources/
COPY local-repo /tmp/local-repo/
WORKDIR /tmp/
RUN mvn package


FROM java:8-jre-alpine as app-de

# reuse args defined above
ARG user
ARG group
ARG uid
ARG gid
ARG app_home
ARG version

RUN mkdir $app_home \
  && addgroup -g $gid $group \
  && chown $uid:$gid $app_home \
  && adduser -D -h "$app_home" -u $uid -G $group $user

COPY --from=buildstage --chown=$uid:$gid /tmp/target/elg-munderline-$version.jar $app_home
COPY --chown=$uid:$gid src/main/resources/models/de-* $app_home/models/
COPY --chown=$uid:$gid src/main/resources/DE_pipeline.conf $app_home/

USER $user
WORKDIR $app_home
# ARG is not available in ENTRYPOINT, but ENV is
ENV version=$version
ENTRYPOINT java -Xms8G -Xmx8G -jar elg-munderline-$version.jar --language=de

EXPOSE 8080


FROM java:8-jre-alpine as app-es

# reuse args defined above
ARG user
ARG group
ARG uid
ARG gid
ARG app_home
ARG version

RUN mkdir $app_home \
  && addgroup -g $gid $group \
  && chown $uid:$gid $app_home \
  && adduser -D -h "$app_home" -u $uid -G $group $user

COPY --from=buildstage --chown=$uid:$gid /tmp/target/elg-munderline-$version.jar $app_home
COPY --chown=$uid:$gid src/main/resources/models/es-* $app_home/models/
COPY --chown=$uid:$gid src/main/resources/ES_pipeline.conf $app_home/

USER $user
WORKDIR $app_home
# ARG is not available in ENTRYPOINT, but ENV is
ENV version=$version
ENTRYPOINT java -Xms4G -Xmx4G -jar elg-munderline-$version.jar --language=es

EXPOSE 8080


FROM java:8-jre-alpine as app-el

# reuse args defined above
ARG user
ARG group
ARG uid
ARG gid
ARG app_home
ARG version

RUN mkdir $app_home \
  && addgroup -g $gid $group \
  && chown $uid:$gid $app_home \
  && adduser -D -h "$app_home" -u $uid -G $group $user

COPY --from=buildstage --chown=$uid:$gid /tmp/target/elg-munderline-$version.jar $app_home
COPY --chown=$uid:$gid src/main/resources/models/el-* $app_home/models/
COPY --chown=$uid:$gid src/main/resources/EL_pipeline.conf $app_home/

USER $user
WORKDIR $app_home
# ARG is not available in ENTRYPOINT, but ENV is
ENV version=$version
ENTRYPOINT java -Xms4G -Xmx4G -jar elg-munderline-$version.jar --language=el

EXPOSE 8080


FROM java:8-jre-alpine as app-en

# reuse args defined above
ARG user
ARG group
ARG uid
ARG gid
ARG app_home
ARG version

RUN mkdir $app_home \
  && addgroup -g $gid $group \
  && chown $uid:$gid $app_home \
  && adduser -D -h "$app_home" -u $uid -G $group $user

COPY --from=buildstage --chown=$uid:$gid /tmp/target/elg-munderline-$version.jar $app_home
COPY --chown=$uid:$gid src/main/resources/models/en-* $app_home/models/
COPY --chown=$uid:$gid src/main/resources/EN_pipeline.conf $app_home/

USER $user
WORKDIR $app_home
# ARG is not available in ENTRYPOINT, but ENV is
ENV version=$version
ENTRYPOINT java -Xms4G -Xmx4G -jar elg-munderline-$version.jar --language=en

EXPOSE 8080
