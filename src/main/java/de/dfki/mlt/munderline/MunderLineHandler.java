package de.dfki.mlt.munderline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.elg.handler.ElgHandler;
import eu.elg.handler.ElgHandlerRegistrar;
import eu.elg.handler.ElgHandlerRegistration;
import eu.elg.handler.HandlerException;
import eu.elg.model.AnnotationObject;
import eu.elg.model.Response;
import eu.elg.model.StatusMessage;
import eu.elg.model.requests.TextRequest;
import eu.elg.model.responses.AnnotationsResponse;

/**
 * Handler for MunderLine requests.
 *
 * @author Jörg Steffen, DFKI
 */
@Component
@ElgHandler
public class MunderLineHandler implements ElgHandlerRegistration {

  private static final Logger logger = LoggerFactory.getLogger(MunderLineHandler.class);

  // MunderLine pipeline
  private MunderLine munderLine;

  // --language command line argument
  @Value("${language:en}")
  private String language;


  /**
   * Process request for annotation with MunderLine.
   *
   * @param request
   *          the request holding the text to annotate
   * @param lang
   *          the language
   * @return annotation response with MunderLine annotation
   * @throws Exception
   *           if an error occurs
   */
  public Response<?> process(TextRequest request, String lang)
      throws Exception {

    String inputText = request.getContent();
    logger.info(String.format("processing text (%,d characters) in language %s",
        inputText.length(), lang));
    if (!this.language.equals(lang.toLowerCase())) {
      String errorMessage = String.format("no model loaded for language \"%s\"; "
          + "available model is for language \"%s\"", lang, this.language);
      logger.error(errorMessage);
      throw new HandlerException(new StatusMessage().withText(errorMessage));
    }

    if (inputText.length() == 0) {
      String errorMessage = ("empty input");
      logger.error(errorMessage);
      throw new HandlerException(new StatusMessage().withText(errorMessage));
    }

    List<String[][]> coNllTables = this.munderLine.processText(inputText, false);

    List<AnnotationObject> textUnits = new ArrayList<>();
    List<AnnotationObject> tokens = new ArrayList<>();

    int inputCharIndex = 0;
    for (String[][] oneAnnotatedSentence : coNllTables) {
      int textUnitStartCharIndex = -1;
      for (String[] oneTokenAnnotation : oneAnnotatedSentence) {
        // get token start and end character index
        String oneToken = oneTokenAnnotation[1];
        int tokenStartCharIndex = getTokenStartCharIndex(oneToken, inputText, inputCharIndex);
        if (tokenStartCharIndex == -1) {
          String errorMessage = (String.format(
              "could not match token \"%s\" in input \"%s\" starting at character index %d",
              oneToken, inputText, inputCharIndex));
          logger.error(errorMessage);
          throw new HandlerException(new StatusMessage().withText(errorMessage));
        }
        int tokenEndCharIndex = tokenStartCharIndex + oneToken.length();
        inputCharIndex = tokenEndCharIndex;
        // set text unit start character index if first token
        if (textUnitStartCharIndex == -1) {
          textUnitStartCharIndex = tokenStartCharIndex;
        }

        AnnotationObject tokenAnnoObj = new AnnotationObject()
            .withOffsets(tokenStartCharIndex, tokenEndCharIndex)
            .withFeature("index", oneTokenAnnotation[0])
            .withFeature("token", oneToken)
            .withFeature("pos", oneTokenAnnotation[3])
            .withFeature("morph", oneTokenAnnotation[5])
            .withFeature("dep-head", oneTokenAnnotation[6])
            .withFeature("dep-rel", oneTokenAnnotation[7]);
        if (oneTokenAnnotation[10] != null) {
          tokenAnnoObj.withFeature("ner", oneTokenAnnotation[10]);
        }
        tokens.add(tokenAnnoObj);
      }
      AnnotationObject textUnitAnnoObj =
          new AnnotationObject().withOffsets(textUnitStartCharIndex, inputCharIndex);
      textUnits.add(textUnitAnnoObj);
      textUnitStartCharIndex = -1;
    }

    logger.info("Done");

    AnnotationsResponse response = new AnnotationsResponse();
    response.withAnnotations("tokens", tokens);
    response.withAnnotations("text_units", textUnits);
    return response;
  }


  /**
   * Get start character index of the given token in the given input string. Start searching
   * at the given search start index.
   *
   * @param token
   *          the token
   * @param input
   *          the input string
   * @param searchStartPos
   *          the character index where to start the search
   * @return the start character index or -1 if search fails
   */
  private static int getTokenStartCharIndex(String token, String input, int searchStartIndex) {

    for (int i = searchStartIndex; i <= input.length() - token.length(); i++) {
      if (matches(i, token, input)) {
        return i;
      }
    }
    return -1;
  }


  /**
   * Attempt to match the given token in the given input string starting at the given character
   * index.
   *
   * @param startCharIndex
   *          character index where to start matching
   * @param token
   *          the token to match
   * @param input
   *          the input string
   * @return <code>true</code> if match succeeds, <code>false</code> otherwise
   */
  private static boolean matches(int startCharIndex, String token, String input) {

    int tokenCharIndex = 0;
    for (int i = startCharIndex; i < startCharIndex + token.length(); i++) {
      if (input.charAt(i) != token.charAt(tokenCharIndex)) {
        return false;
      }
      tokenCharIndex++;
    }
    return true;
  }


  @Override
  public void registerHandlers(ElgHandlerRegistrar registrar) {

    try {
      // load model according to passed --language command line argument and register handler
      this.language = this.language.toLowerCase();
      logger.info(String.format("loading models for language \"%s\"...", this.language));
      switch (this.language) {
        case "de":
          this.munderLine = new MunderLine("DE_pipeline.conf");
          break;
        case "en":
          this.munderLine = new MunderLine("EN_pipeline.conf");
          break;
        case "es":
          this.munderLine = new MunderLine("ES_pipeline.conf");
          break;
        case "el":
          this.munderLine = new MunderLine("EL_pipeline.conf");
          break;
        default:
          String errorMessage = String.format("unsupported language \"%s\"", this.language);
          logger.error(errorMessage);
          throw new HandlerException(new StatusMessage().withText(errorMessage));
      }
      registrar.handler("/" + this.language, TextRequest.class, r -> process(r, this.language));
      logger.info(String.format("registered language \"%s\"", this.language));
    } catch (ConfigurationException | IOException e) {
      logger.error(e.getLocalizedMessage(), e);
      throw new HandlerException(new StatusMessage().withText(e.getLocalizedMessage()));
    }
  }
}
